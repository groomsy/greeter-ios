Greeter
=======

Greeter is a simple sample iOS project built to demo a few aspects of iOS development:

- Setting up a Project
- Properties
- Delegates
- Storyboards
- Transferring Data between Storyboards
- Localization

This project is meant to behave as a simple exercise for developers new to the iOS platform.

Screenshots
-----------

![Form Screen](https://dl.dropbox.com/u/749855/GreeterScreens/form-screen.png)
![Greet Screen](https://dl.dropbox.com/u/749855/GreeterScreens/greet-screen.png)

Tutorial
--------

If you would like a written tutorial for this lesson, please contact me at todd(dot)grooms(at)gmail(dot)com.

License
-------

Copyright (c) 2013 Todd Grooms

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
