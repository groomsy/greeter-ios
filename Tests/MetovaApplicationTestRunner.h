//
//  MetovaApplicationTestRunner.h
//  Metova
//
//  Created by Todd Grooms on 10/25/12.
//  Copyright (c) 2012 Metova, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MetovaApplicationTestRunner : UIApplication

@end
