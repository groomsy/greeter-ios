//
//  UIButton+GHTest.m
//  MetovaArchetype
//
//  Created by Todd Grooms on 3/8/13.
//  Copyright (c) 2013 Metova, Inc. All rights reserved.
//

#import "UIButton+GHTest.h"

#import <GHUnitIOS/GHUnit.h>

@implementation UIButton (GHTest)

- (BOOL)isButtonWiredToTarget:(id)target forIBActionSelector:(SEL)ibactionSelector forControlEvent:(UIControlEvents)controlEvent
{
    NSString *actionSelectorString = NSStringFromSelector(ibactionSelector);

    NSArray *actionsForTarget = [self actionsForTarget:target forControlEvent:controlEvent];
    for ( NSString *action in actionsForTarget )
    {
        if ( [actionSelectorString isEqualToString:action] == YES )
        {
            return YES;
        }
    }

    return NO;
}


@end
