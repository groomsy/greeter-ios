//
//  main.m
//  Tests
//
//  Created by Todd Grooms on 4/21/13.
//  Copyright (c) 2013 Groomsy Dev. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, @"MetovaApplicationTestRunner", @"GHUnitIOSAppDelegate");
    }
}


FILE *fopen$UNIX2003(const char *filename, const char *mode)
{
    return fopen(filename, mode);
}


size_t fwrite$UNIX2003(const void *ptr, size_t size, size_t nitems, FILE *stream)
{
    return fwrite(ptr, size, nitems, stream);
}
