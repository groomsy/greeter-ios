//
//  UIViewUtils.h
//  MetovaArchetype
//
//  Created by Todd Grooms on 3/8/13.
//  Copyright (c) 2013 Metova, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIViewUtils : NSObject

+ (UIButton *)buttonForAccessibilityLabel:(NSString *)accessibilityLabel fromView:(UIView *)view;

@end
