//
//  FormViewControllerTests.m
//  Greeter
//
//  Created by Todd Grooms on 4/21/13.
//  Copyright (c) 2013 Groomsy Dev. All rights reserved.
//

#pragma mark Imports
#import <GHUnitIOS/GHUnit.h>
#import <OCMock/OCMock.h>

#import "FormViewController.h"
#import "UIButton+GHTest.h"
#import "UIViewUtils.h"

#pragma mark -

#pragma mark Categories
@interface FormViewController (UnitTest)

- (UILabel *)instructionLabel;
- (UITextField *)nameTextField;
- (UIButton *)greetButton;

@end

#pragma mark -

@interface FormViewControllerTests : GHTestCase
{
    UINavigationController *navigationController;
    FormViewController *formViewController;
}

@end

@implementation FormViewControllerTests

- (BOOL)shouldRunOnMainThread
{
    return YES;
}


- (void)setUp
{
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"MainStoryboard"
                                                             bundle:nil];
    formViewController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"FormViewController"];

    navigationController = [[UINavigationController alloc] initWithRootViewController:formViewController];

    [formViewController loadView];
    [formViewController viewDidLoad];
    [formViewController viewWillAppear:NO];
    [formViewController viewDidAppear:NO];
}


- (void)tearDown
{
    formViewController = nil;

    navigationController = nil;
}


- (void)testInstructionsDisplayedCorrectly
{
    GHAssertNotNil([formViewController instructionLabel], @"FormViewController's instructionLabel is nil!");
    GHAssertEqualStrings([[formViewController instructionLabel] text], NSLocalizedString(@"InstructionLabelText", nil), @"FormViewController's instructionLabel does not contain the correct text.");
}


- (void)testNameTextFieldDisplayedCorrectly
{
    GHAssertNotNil([formViewController nameTextField], @"FormViewController's nameTextField is nil!");
    GHAssertEqualStrings([[formViewController nameTextField] placeholder], NSLocalizedString(@"NameTextFieldPlaceholder", nil), @"FormViewController's nameTextField does not contain the correct placeholder text.");
}


- (void)testGreetButtonDisplayedCorrectly
{
    UIButton *greetButton = [UIViewUtils buttonForAccessibilityLabel:@"Greet Button" fromView:[formViewController view]];
    GHAssertNotNil(greetButton, @"FormViewController's greetButton is nil!");
    GHAssertEquals(greetButton, [formViewController greetButton], @"The greetButton found in the view is not the same as FormViewController's greetButton getter.");

    GHAssertEqualStrings([[greetButton titleLabel] text], NSLocalizedString(@"GreetButtonTitle", nil), @"FormViewController's greetButton does not have the correct title.");

    GHAssertTrue([greetButton isButtonWiredToTarget:formViewController forIBActionSelector:@selector(greet:) forControlEvent:UIControlEventTouchUpInside], @"The greetButton is not wired to the greet: selector in the FormViewController.");
}


- (void)testSeguePerformedOnGreet
{
    id mockFormViewController = [OCMockObject partialMockForObject:formViewController];

    [[mockFormViewController expect] performSegueWithIdentifier:@"GreetSegue" sender:formViewController];

    [mockFormViewController greet:[mockFormViewController greetButton]];

    [mockFormViewController verify];
}


@end
