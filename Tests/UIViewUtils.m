//
//  UIViewUtils.m
//  MetovaArchetype
//
//  Created by Todd Grooms on 3/8/13.
//  Copyright (c) 2013 Metova, Inc. All rights reserved.
//

#import "UIViewUtils.h"

@implementation UIViewUtils

+ (UIButton *)buttonForAccessibilityLabel:(NSString *)accessibilityLabel fromView:(UIView *)view
{
    BOOL ( ^isButtonForAccessibilityLabel)(UIView *) = ^(UIView * view) {
        if ( [view isKindOfClass:[UIButton class]] == YES )
        {
            UIButton *button = (UIButton *)view;
            return [button.accessibilityLabel isEqualToString:accessibilityLabel];
        }

        return NO;
    };

    UIView *returnedView = [UIViewUtils returnView:view satisfiesBlock:isButtonForAccessibilityLabel];
    if ( returnedView != nil && [returnedView isKindOfClass:[UIButton class]] == YES )
    {
        return (UIButton *)returnedView;
    }

    return nil;
}

+ (UIView *)returnView:(UIView *)view satisfiesBlock:( BOOL ( ^)(UIView *) )satisfiesBlock
{
    if ( satisfiesBlock(view) == YES )
    {
        return view;
    }

    NSArray *subviews = [view subviews];
    for ( UIView *subview in subviews )
    {
        UIView *returnView = [UIViewUtils returnView:subview satisfiesBlock:satisfiesBlock];
        if ( returnView != nil )
        {
            return returnView;
        }
    }

    return nil;
}


@end
