//
//  UIButton+GHTest.h
//  MetovaArchetype
//
//  Created by Todd Grooms on 3/8/13.
//  Copyright (c) 2013 Metova, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (GHTest)

- (BOOL)isButtonWiredToTarget:(id)target forIBActionSelector:(SEL)ibactionSelector forControlEvent:(UIControlEvents)controlEvent;

@end
