//
//  MetovaApplicationTestRunner.m
//  Metova
//
//  Created by Todd Grooms on 10/25/12.
//  Copyright (c) 2012 Metova, Inc. All rights reserved.
//

#import "MetovaApplicationTestRunner.h"

#import <GHUnitIOS/GHUnit.h>

@implementation MetovaApplicationTestRunner

- (id)init
{
    self = [super init];

    /*
     * There is a weird quirk in 6.0+ that prevents the tests from running. This essentially
     * forces the tests to run if the OS is 6.0 or later.
     * - tgrooms
     */
    if ( getenv("GHUNIT_CLI") && [[[UIDevice currentDevice] systemVersion] hasPrefix:@"6."] )
    {
        int failures = [GHTestRunner run];
        if ( failures > 0 )
        {
            NSLog(@"%i Failures exist.", failures);
        }
    }

    return self;
}


@end
