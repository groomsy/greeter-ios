//
//  main.m
//  Greeter
//
//  Created by Todd Grooms on 1/10/13.
//  Copyright (c) 2013 Groomsy Dev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain( argc, argv, nil, NSStringFromClass([AppDelegate class]) );
    }
}
