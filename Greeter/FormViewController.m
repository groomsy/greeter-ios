//
//  FormViewController.m
//  Greeter
//
//  Created by Todd Grooms on 1/10/13.
//  Copyright (c) 2013 Groomsy Dev. All rights reserved.
//

#pragma mark Imports
#import "FormViewController.h"

#pragma mark UI
#import "GreetViewController.h"

#pragma mark -

@interface FormViewController ()

- (UILabel *)instructionLabel;
- (UITextField *)nameTextField;
- (UIButton *)greetButton;

@end

@implementation FormViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [instructionLabel setText:NSLocalizedString(@"InstructionLabelText", nil)];
    [nameTextField setPlaceholder:NSLocalizedString(@"NameTextFieldPlaceholder", nil)];
    [greetButton setTitle:NSLocalizedString(@"GreetButtonTitle", nil) forState:UIControlStateNormal];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"GreetSegue"] )
    {
        UIViewController *destinationViewController = [segue destinationViewController];
        if ( [destinationViewController isKindOfClass:[GreetViewController class]] )
        {
            GreetViewController *greetViewController = (GreetViewController *)destinationViewController;
            [greetViewController setName:[nameTextField text]];
        }
    }
}


#pragma mark IBAction Methods
- (IBAction)greet:(id)sender
{
    [self performSegueWithIdentifier:@"GreetSegue" sender:self];
}


#pragma mark UITextField Delegate Methods
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    NSLog(@"Should end editing of textField");
    return [[textField text] length] > 0;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"Should return on textField");
    [textField endEditing:YES];
    return NO;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"Did end editing of textField");
    [greetButton setEnabled:YES];
}


#pragma mark Getter Methods
- (UILabel *)instructionLabel
{
    return instructionLabel;
}


- (UITextField *)nameTextField
{
    return nameTextField;
}


- (UIButton *)greetButton
{
    return greetButton;
}


@end
