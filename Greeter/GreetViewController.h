//
//  GreetViewController.h
//  Greeter
//
//  Created by Todd Grooms on 1/10/13.
//  Copyright (c) 2013 Groomsy Dev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GreetViewController : UIViewController
{
    IBOutlet UILabel *greetingLabel;
}

@property (nonatomic, strong) NSString *name;

@end
