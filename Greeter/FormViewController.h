//
//  FormViewController.h
//  Greeter
//
//  Created by Todd Grooms on 1/10/13.
//  Copyright (c) 2013 Groomsy Dev. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    RequestType_Post,
    RequestType_Get
}
RequestType;

@interface FormViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet UILabel *instructionLabel;
    IBOutlet UITextField *nameTextField;
    IBOutlet UIButton *greetButton;
}

- (IBAction)greet:(id)sender;

@end
