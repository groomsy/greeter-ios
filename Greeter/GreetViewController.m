//
//  GreetViewController.m
//  Greeter
//
//  Created by Todd Grooms on 1/10/13.
//  Copyright (c) 2013 Groomsy Dev. All rights reserved.
//

#pragma mark Imports
#import "GreetViewController.h"

#pragma mark -

@implementation GreetViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [greetingLabel setText:[NSString stringWithFormat:NSLocalizedString(@"FormattedGreeting", nil), _name]];
}


@end
